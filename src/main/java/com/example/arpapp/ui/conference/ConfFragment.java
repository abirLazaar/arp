package com.example.arpapp.ui.conference;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.arpapp.R;

public class ConfFragment extends Fragment {

    private ConfViewModel confViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        confViewModel =
                ViewModelProviders.of(this).get(ConfViewModel.class);
        View root = inflater.inflate(R.layout.fragment_conference, container, false);
        final TextView textView = root.findViewById(R.id.text_conf);
        confViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}