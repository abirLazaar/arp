package com.example.arpapp.ui.conference;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ConfViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ConfViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Conference fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}