package com.example.arpapp.ui.newsletter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class NewsletterViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public NewsletterViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Newsletter fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}