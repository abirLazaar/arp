package com.example.arpapp.ui.newsletter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.arpapp.R;
import com.github.barteksc.pdfviewer.PDFView;

public class NewsletterFragment extends Fragment {

    private NewsletterViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(NewsletterViewModel.class);
        View root = inflater.inflate(R.layout.fragment_newsletter, container, false);
        final PDFView pdfView = root.findViewById(R.id.pdfView);
        pdfView.fromAsset("newsletter.pdf").load();

        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });
        return root;
    }
}