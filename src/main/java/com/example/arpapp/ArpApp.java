package com.example.arpapp;

import android.app.Application;

public class ArpApp extends Application {
    private boolean isFrenshMode = true;

    public boolean isFrenshMode() {
        return isFrenshMode;
    }
    public void setFrenshMode(boolean frenshMode) {
        isFrenshMode = frenshMode;
    }
}
