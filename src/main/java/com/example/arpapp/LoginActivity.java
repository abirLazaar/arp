package com.example.arpapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private ArpApp arpApp;
    private Button languageFr,languageAr,btnLogin;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        languageAr=(Button)findViewById(R.id.lang_ar);
        languageFr=(Button)findViewById(R.id.lang_fr);
        btnLogin=(Button)findViewById(R.id.btn_login);
        //get application class
        arpApp = (ArpApp) getApplication();

        languageAr.setOnClickListener(this);
        languageFr.setOnClickListener(this);
        btnLogin.setOnClickListener(this);

        if (arpApp.isFrenshMode()) {
            languageFr.setBackgroundResource(R.drawable.bg_toggle_left_checked);
            languageAr.setBackgroundResource(R.drawable.bg_toggle_right);
            languageFr.setTextColor(getResources().getColor(R.color.colorWhite));
            languageAr.setTextColor(getResources().getColor(R.color.colorPrimary));
        } else {
            changeLanguage("ar", this);
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            languageFr.setBackgroundResource(R.drawable.bg_toggle_left);
            languageAr.setBackgroundResource(R.drawable.bg_toggle_right_checked);
            languageFr.setTextColor(getResources().getColor(R.color.colorPrimary));
            languageAr.setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lang_ar:
                arpApp.setFrenshMode(false);
                changeLanguage("ar", this);
                languageFr.setBackgroundResource(R.drawable.bg_toggle_left);
                languageAr.setBackgroundResource(R.drawable.bg_toggle_right_checked);
                reopenActivity(LoginActivity.this,arpApp.isFrenshMode());
                languageFr.setTextColor(getResources().getColor(R.color.colorWhite));
                languageAr.setTextColor(getResources().getColor(R.color.colorPrimary));
                break;
            case R.id.lang_fr:
                arpApp.setFrenshMode(true);
                changeLanguage("fr", this);
                languageFr.setBackgroundResource(R.drawable.bg_toggle_left_checked);
                languageAr.setBackgroundResource(R.drawable.bg_toggle_right);
                reopenActivity(LoginActivity.this,arpApp.isFrenshMode());
                languageFr.setTextColor(getResources().getColor(R.color.colorPrimary));
                languageAr.setTextColor(getResources().getColor(R.color.colorWhite));
                break;
            case R.id.btn_login:
                startActivity(new Intent(LoginActivity.this, MenuActivity.class));
                this.overridePendingTransition(0, 0);
                finish();
                break;

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static void changeLanguage(String languageToLoad, Context context)
    {
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = context.getResources().getConfiguration();
        config.setLocale(locale);
        context.createConfigurationContext(config);
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static void reopenActivity(Activity activity, boolean language) {
        activity.finish();
        Intent intent = new Intent(activity, activity.getClass());
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        //set layout direction
        if(language)
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        else
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

        activity.overridePendingTransition(0, 0);
        activity.startActivity(intent);
        activity.overridePendingTransition(0, 0);
    }
}